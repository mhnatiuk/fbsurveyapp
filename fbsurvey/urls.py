from django.conf.urls import patterns, include, url
import views


urlpatterns = patterns('',
    url(r'^collect/$', views.collect_user_info, name='collect_user_info'),
)
