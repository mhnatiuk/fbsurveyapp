from django.db import models
from json import decoder
# Create your models here.


def decode_json(json_object):
    

class Respondent(models.Model):
    fb_id = models.CharField(max_length=1000, blank=False, unique=True, db_index=True)
    name = models.CharField(max_length=1000, db_index=True)
    first_name = models.CharField(max_length=1000, db_index=True)
    middle_name = models.CharField(max_length=1000, db_index=True)
    last_name = models.CharField(max_length=1000, db_index=True)
    email = models.CharField(max_length=1000, db_index=True)
    gender = models.CharField(max_length=100, db_index=True)
    timezone = models.CharField(max_length=1000, db_index=True)
    verified = models.CharField(max_length=100, db_index=True)
    locale = models.CharField(max_length=1000, db_index=True)
    link = models.CharField(max_length=1000, db_index=True)
    updated_time = models.CharField(max_length=1000, db_index=True)

    def save_json(self, json_object):
        decode_json(json_object)
        super(Respondent, self).save(*args, **kwargs)


class Post(models.Model):
    """
    placeholder for FB post of Respondent.
    post.text
    post.urls -> Url
    """
    pass

class UrlsInPost(models.Model):
    """ placeholder for urls in user's posts """
    pass