from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'FBSurveyApp.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'fbcollect.views.index', name="index"),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^fbcollect/', include('fbcollect.urls')),
    # questionnaire urls
    url(r'q/', include('questionnaire.urls')),
    url(r'dziekujemy', 'fbcollect.views.thanks'),
    url(r'about', 'fbcollect.views.about'),
    url(r'privacy', 'fbcollect.views.privacy'),
    url(r'^take/(?P<questionnaire_id>[0-9]+)/$', 'questionnaire.views.generate_run'),
#    url(r'^/$', 'questionnaire.page.views.page', {'page_to_render' : 'index'}),
#    url(r'^$', 'questionnaire.page.views.page', {'page_to_render' : 'index'}),
    url(r'^(?P<lang>..)/(?P<page_to_trans>.*)\.html$', 'questionnaire.page.views.langpage'),
    url(r'^(?P<page_to_render>.*)\.html$', 'questionnaire.page.views.page'),
    url(r'^setlang/$', 'questionnaire.views.set_language'),

)
