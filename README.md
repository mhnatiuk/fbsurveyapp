# FBSurveyApp #

This is a Python package for surveys over Facebook with FB data collection included. This work was supported by the National Science Centre ([Narodowe Centrum Nauki](https://www.ncn.gov.pl/)) under Grant 2012/05/B/HS6/03802.


### How do I get set up? ###

You need running LimeSurvey instance for this project to run. Alternativly, you can configure it to run seantis-questionnaire python module and run whole survey using this app, but seantis questionnaire question-type set is quite limited


### Who do I talk to? ###
contact mikolaj.hnatiuk@gmail.com for assistance in using this package