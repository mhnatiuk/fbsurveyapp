from django.contrib import admin
from models import User
# Register your models here.

adminsite = admin.site

class UserAdmin(admin.ModelAdmin):
    pass

adminsite.register(User, UserAdmin)
