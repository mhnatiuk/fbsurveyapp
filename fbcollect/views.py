import json
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.http import HttpResponseForbidden
from django.http import HttpResponseBadRequest
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import ensure_csrf_cookie
from django.template import RequestContext
from django.core.context_processors import csrf
from django.views.decorators.csrf import requires_csrf_token
from models import User
import re



@ensure_csrf_cookie
def index(request):
    context = RequestContext(request, {'check': True })
    return render(request, 'fbcollect/index.html')


def thanks(request):
    context = RequestContext(request)
    return render(request, 'fbcollect/thanks.html')

def about(request):
    context = RequestContext(request)
    return render(request, 'fbcollect/about.html')

def privacy(request):
    context = RequestContext(request)
    return render(request, 'fbcollect/privacy.html')




# Create your views here.
def csrf_failure(request, reason=""):
    #print request
    return HttpResponseForbidden(request)
def is_valid_json(jsonstr):
    return type(json.loads(jsonstr)) == dict
def collect(request):
    
    if request.method != "POST" or request.is_ajax() == False:
        return HttpResponseForbidden()
    
    if request.POST is None:
        return HttpResponseBadRequest()
    post_data = json.loads(request.body)
    if re.search(r'[^0-9]', post_data['uid']) != None:
        return HttpResponse(json.dumps({"success" : False }), content_type="application/json")
    user, created = User.objects.get_or_create(fb_uid=post_data['uid'])
    if created:
        user.fb_uid = post_data['uid']
        if not is_valid_json(post_data['data']):
            return HttpResponse(json.dumps({"success" : False }), content_type="application/json")
        user.data = json.dumps(post_data['data'])
        user.save()
    else:
        user.data = json.dumps(post_data['data'])
        user.save()
    params = {"success" : True, "created" : created }
    return HttpResponse(json.dumps(params), content_type="application/json")
    
