// Declare angular module for the main application
angular.module('ngNCNSurvey', ['ngCookies', 'ngFacebook']).

config(function($facebookProvider){
    $facebookProvider.setAppId('489167387890595'); // set facebook application ID
    //configure permissions for data collection
    $facebookProvider.setPermissions("public_profile,user_friends,user_hometown,user_likes,user_location,user_posts,user_religion_politics");
}).

config(function($interpolateProvider){
    // django template tags are the same, need to reconfigure AngularJS
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
    
}).
config(function($httpProvider){
    // set cross site request forgery
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    //$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    //$httpProvider.defaults.xsrfCookieName = 'csrftoken';
    
})

// set up facebook javascript SDK
.run( function( $rootScope, $http, $cookies) {
  // Cut and paste the "Load the SDK" code from the facebook javascript sdk page.
  $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
  // Load the facebook SDK asynchronously
 (function(){
     // If we've already installed the SDK, we're done
     if (document.getElementById('facebook-jssdk')) {return;}
     // Get the first script element, which we'll use to find the parent node
     var firstScriptElement = document.getElementsByTagName('script')[0];
     // Create a new script element and set its id
     var facebookJS = document.createElement('script'); 
     facebookJS.id = 'facebook-jssdk';
     // Set the new script's source to the source of the Facebook JS SDK
     facebookJS.src = '//connect.facebook.net/en_US/all.js';
     // Insert the Facebook JS SDK into the DOM
     firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
   }());
})
;


// main controller. handle data collection and redirection to survey after user logs into the appliaction
var DemoCtrl = function ($scope, $facebook, $http, $timeout) {
    $data = { "number_of_friends" : NaN };
    $scope.isLoggedIn = false;
      $scope.login = function() {
        $facebook.login().then(function() {    
          refresh();
        });
      }
      
   
  function saveData(uid, userdata) {
    console.log(userdata);
    $http.post("/fbcollect/collect/", {"uid": uid, "data": userdata}).
        success(function(data, status, headers, config) {
            console.log(data);
            redirect($meid);
            } ).
        error(function(data, status, headers, config) {
            //alert("no");
            console.log(data);
            });
  }
  function redirect(linkid){
    window.location = "http://infobadanie.icm.edu.pl/limesurvey/index.php?r=survey/index&sid=346194&lang=pl&q4info="+linkid;
  }
  function refresh() {
    
    
    
    $meid ="";
    $facebook.api("/me").then( 
      function(response) {
        $scope.welcomeMsg = "Zalogowano jako:" + response.name;
        $meid = response.id;
      },
      function(err) {
        $scope.welcomeMsg = "Proszę się zalogować";
      });
    // public_profile,user_friends,user_hometown,user_likes,user_location,user_posts,user_religion_politics
    //infolist = ["posts", "likes", "friends"];
    
    since_date = (Date.parse('2015-01-01'))/1000
    $facebook.api("/me/posts?limit=1000&since=" + since_date).then(
        function(response){
            if (!$data.posts) {
              //  response.data.forEach(function(d) { $data.posts.push(d); } );
              $data.posts = response.data;
            }
        $facebook.api("/me/likes?limit=2000&since=" + since_date).then(
        function(response){
            if (!$data.likes) {
             //   response.data.forEach(function(d) { $data.likes.push(d); } );
             $data.likes = response.data;
            }
            $facebook.api("/me/friends").then(
                        function(response){
                            if (!$data.friends) {
                                $data.friends = [];
                                response.data.forEach(function(d) { $data.friends.push(d); } );
                            }
                            $data.number_of_friends = response.summary.total_count
                            $scope.isLoggedIn = true;
                            saveData($meid, $data);
                    });
            }); 
    });
    
    }
  refresh();
};
